<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Ajax extends Controller
{
    public function before()
    {
        parent::before();
        if( ! $this->request->is_ajax())
        {
            throw new HTTP_Exception_404('file not found');
        }
    }
        
    public function action_get_json()
    {
        $info = Arr::get($_POST, 'info', NULL);
        $param = Arr::get($_POST, 'param', NULL);
        
        $routine = Helper_Routine::choice_routine($info);
        
        $result = Helper_Routine::call($routine, $param);
        
        echo json_encode($result);
    }
    public function action_get_doctor_shedule()
    {
        $doctor_id = Arr::get($_POST, 'doctor_id', NULL);
        $date = Arr::get($_POST, 'date', NULL);
        
        $d = new Model_Doctor; 
        $schedule = $d->get_doctor_shedule($doctor_id, $date);
        
        foreach($schedule as $record)
        {
            foreach($record as $key => $value)
            {
                ${$key}[] = $value; // Genius. Походу очень изжоп, но работает
            }           
        }
        
        echo json_encode(array(
            'patient_id' => $patient_id,
            'time' => $time,
            'info' => $info
            ));
          
    }
    public function action_get_patient_records()
    {
        $patient_id = Arr::get($_POST, 'patient_id', NULL);
        
        $p = new Model_Patient;
        $records = $p->get_patient_records($patient_id);
        if( ! $records)
            return NULL;
        foreach($records as $record)
        {
            foreach($record as $key => $value)
            {
                ${$key}[] = $value; // Genius. Походу очень изжоп, но работает
            }           
        }
        
        echo json_encode(array(
            'record_id' => $record_id,
            'name' => $name,
            'specialization' => $specialization,
            'date' => $date,
            'time' => $time
            ));
        
    }
}