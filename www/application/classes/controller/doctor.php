<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Doctor extends Controller_Common {
 
    public function action_index()
    {
        
             
        $scripts = Helper_File::get_scripts(array('doctor', 'ajax_functions', 'patient_callback'));

        $content = View::factory('doctor');
        $this->template->scripts = $scripts;
        $this->template->title = 'Врач';
        $this->template->content = $content;
        
    }
    
}