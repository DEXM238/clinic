<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Patient extends Controller_Common {
 
    public function action_info()
    {
        
        $patient_id = $this->request->param('id', NULL);
        $scripts = Helper_File::get_scripts(array('ajax_functions'));
        
        $p = new Model_Patient;
        $card = $p->get_patient_cards($patient_id);

        $content = View::factory('patient_info')
                ->set('card', $card);
        $this->template->scripts = $scripts;
        $this->template->title = 'Сведения о пациенте';
        $this->template->content = $content;
        
    }
   
} 