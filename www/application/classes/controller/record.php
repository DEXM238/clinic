<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Record extends Controller_Common {
 
    public function action_index()
    {
        $data = arr::extract($_POST, array('time', 'patient_id', 'doctor_id', 'doctor_name',  'info'), NULL);

        
        if(isset($data['time']))
        {
            $r = new Model_Record;
            $r->add_record($data['doctor_id'], $data['patient_id'], $data['info'], $data['time']);
        }
             
        $scripts = Helper_File::get_scripts(array('ajax_record', 'ajax_functions', 'patient_callback'));

        $content = View::factory('record')
                ->set('patient_id', $data['patient_id'])
                ->set('doctor_name', $data['doctor_name']);
        $this->template->scripts = $scripts;
        $this->template->title = 'Регистратор';
        $this->template->content = $content;
        
    }
    public function action_change()
    {
        $records = Arr::get($_POST, 'records');
        
        if(isset($records))
        {
            $r = new Model_Record;
            $r->change_record_status($records, 'P');
        }
                
        $scripts = Helper_File::get_scripts(array('ajax_change_record', 'ajax_functions', 'patient_callback'));
        $content = View::factory('change_record');
        
        $this->template->scripts = $scripts;
        $this->template->title = 'Отмена записей';
        $this->template->content = $content;
    }
} // End Record