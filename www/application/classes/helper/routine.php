<?php

    class Helper_Routine
    {
        public static function call($routine, $param, $flag = 'current')
        {
            $result = DB::query(Database::SELECT, 
                            'CALL '.$routine.'(:param)')
                                ->param(':param', $param)
                                //->param(':routine', $routine)
                                ->execute();
            if($flag === 'as_array')
            {
                $result = $result->as_array();
                return $result[0];// костыль!!!
            }
            elseif($flag === 'current')
            {
                return $result->current();
            }
        }
        public static function choice_routine($info)
        {
            
            $routines = array(
                'short_doctor_by_name' => 'get_short_doctor_by_name',
                'short_patient_by_id' => 'get_short_patient_by_id',
                'short_patient_by_name' => 'get_short_patient_by_name',
                'patient_card' => 'get_patient_card',
            );
            
            return $routines[$info];
        }
    }

 
?>
