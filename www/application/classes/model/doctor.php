﻿<?php defined('SYSPATH') or die('No direct script access.');

class Model_Doctor
{
	public function get_doctor_shedule($doctor_id, $date)
        {
            $result = DB::query(Database::SELECT, 'CALL get_doctor_schedule(:date, :doctor_id)')
                    ->param(':doctor_id', $doctor_id)
                    ->param(':date', $date)
                    ->execute()
                    ->as_array();
            return $result;
            $cell = array();
            foreach($result as $item)
            {
                $cell[] = $item['time'];
            }
            return $cell;
        }
       
} // End 
