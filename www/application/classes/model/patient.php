﻿<?php defined('SYSPATH') or die('No direct script access.');

class Model_Patient
{
	public function get_patient_records($patient_id)
        {
            $result = DB::query(Database::SELECT, 'CALL get_patient_records(:patient_id)')
                    ->param(':patient_id', $patient_id)
                    ->execute()
                    ->as_array();

            return $result;
        }
        public function get_patient_cards($patient_id)
        {
            return Helper_Routine::call('get_patient_card', $patient_id, 'as_array');
        }
       
} // End 
