﻿<?php defined('SYSPATH') or die('No direct script access.');

class Model_Record
{
        public function add_record($doctor_id, $patient_id, $info, $date)
        {
            return DB::query(Database::INSERT, 'CALL ins_record(:doctor_id, :patient_id, :info, :date)')
                    ->param(':doctor_id', $doctor_id)
                    ->param(':patient_id', $patient_id)
                    ->param(':info', $info)
                    ->param(':date', $date)
                    ->execute();
        }
        public function change_record_status($records_id, $status)
        {
			//$record_id - массив идентификаторов записи, которым нужно изменить статус
            $query = '(';
            foreach($records_id as $record_id)
            {
                $query.=$record_id.', ';
            }
            $query = substr($query, 0, strlen($query)-2);
            $query.=')';
            return DB::query(Database::UPDATE, 'UPDATE records SET status = :status WHERE record_id IN'.$query)
                    ->param(':status', $status)
                    ->execute();
        }
        
} // End 
