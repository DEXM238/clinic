<div class="row-fluid">
    <div class="span8">
        <form class="form" method="post">
            <fieldset>
                <legend>Выбор пациента:</legend>
                <div class="control-group">
                    <div class="controls">
                        
                    </div>
                </div>
                <div class="control-group" id="patient_group">
                    <div class="controls">
                        <input  tabindex="1" type="text" class="input-xlarge" id="patient_name" placeholder="ФИО пациента">
                        <input  tabindex="1" type="text" class="input span2" id="patient_id"  name="patient_id" placeholder="Код пациента">
                        <span class="help-inline">Пациент не найден!</span>
                    </div>
                </div>
            </fieldset>
            <fieldset>
                <legend>Записи пациента:</legend>
                <div class="control-group" id="patient_records_group">
                </div>
            </fieldset>
            
            <div class="form-actions">
                <button type="submit" class="btn btn-primary">Отменить запись</button>
                <button type="reset" class="btn">Сбросить</button>
            </div>       
            <input type="hidden" name="time" id="time">
            <input type="hidden" name="doctor_id" id="doctor_id">
        </form>
    </div>
        <div class="span4" id="patient_info">  
            
        </div>
       
</div>