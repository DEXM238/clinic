<div class="row-fluid">
    <div class="span8">
        <form class="form" method="post">
            <fieldset>
                <legend>Записи пациентов:</legend>
                <div class="control-group">
                    <div class="controls">
                        <input type="text" class="input span2" id="datepicker" placeholder="Дата приема">  
                        <a href="#" id="showHideContent">Скрыть расписание</a>

                            <?php echo View::factory('schedule'); ?>
                    </div>
                </div>
            </fieldset>
            <fieldset>
                <legend>Автор записи:</legend>
                <div class="control-group">
                    <div class="controls">
                        <input type="text" class="input-xlarge" placeholder="ФИО врача">  
                    </div>
                </div>
            </fieldset>
            <fieldset>
                <legend>Прием пациента:</legend>              
                <div class="control-group">
                    <div class="controls">
                        <textarea name="info" class="input-xxlarge" id="patient_extra" rows="6" placeholder="Обследование пациента"></textarea>
                    </div>
                    <div class="controls">
                        <textarea name="info" class="input-xlarge" id="patient_extra" rows="3" placeholder="Рецепт"></textarea>
                    </div>
                </div>
            </fieldset>
            <div class="form-actions">
                <button type="submit" class="btn btn-primary">Добавить запись</button>
                <button type="reset" class="btn">Сбросить</button>
                <a class="btn btn-info" target="_blank" href="/record" id="record" >Назначить повторный прием</a>                     
            </div>       
            <input type="hidden" name="doctor_id" id="doctor_id">
        </form>
        <form class="form" method="post"  name="da1ta" id="da1ta" action="/record" target="_blank">
            <input type="hidden" name="patient_id" id="patient_id">
            <input type="hidden" name="doctor_name" value="Ерашев Вячеслав Федорович">
        </form>
    </div>
        <div class="span4" id="patient_info">  
            
        </div>
     <div class="span4" id="info">  
            
        </div>
</div>