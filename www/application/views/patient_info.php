    <ul class="nav nav-tabs">
    <li class="active">
    <a href="#">Медицинская карта</a>
    </li>
    <li><a href="#">Медицинские записи</a></li>
    </ul>
<?php echo  View::factory('property_div')
        ->set('name', 'Контингенти')
        ->set('id', 'privilege')
        ->set('value', $card['privilege'])?>
<?php echo  View::factory('property_div')
        ->set('name', 'Группа крові')
        ->set('id', 'blood')
        ->set('value', $card['blood'])?>
<?php echo  View::factory('property_div')
        ->set('name', 'Переливання крові(коли, скільки)')
        ->set('id', 'blood_transfusion')
        ->set('value', $card['blood_transfusions'])?>
<?php echo  View::factory('property_div')
        ->set('name', 'Цукровий діабет')
        ->set('id', 'diabete')
        ->set('value', $card['diabete'])?>
<?php echo  View::factory('property_div')
        ->set('name', 'Інфекційні захворювання')
        ->set('id', 'infections')
        ->set('value', $card['infections'])?>
<?php echo  View::factory('property_div')
        ->set('name', 'Алергологічний анамнез')
        ->set('id', 'allergy')
        ->set('value', $card['allergies'])?>
<?php echo  View::factory('property_div')
        ->set('name', 'Непереносимість до лікарських препаратів')
        ->set('id', 'medications')
        ->set('value', $card['medications'])?>
