<div class="row-fluid">
    <div class="span8">
        <form class="form" method="post">
            <fieldset>
                <legend>Выбор пациента:</legend>
                <div class="control-group">
                    <div class="controls">
                        
                    </div>
                </div>
                <div class="control-group" id="patient_group">
                    <div class="controls">
                        <input  tabindex="1" type="text" class="input-xlarge" id="patient_name" placeholder="ФИО пациента">
                        <input  tabindex="1" type="text" class="input span2" id="patient_id"  name="patient_id" placeholder="Код пациента" value="<?php if(isset($patient_id)) echo $patient_id; ?>">
                        <span class="help-inline">Пациент не найден!</span>
                    </div>
                </div>
                <div class="control-group">
                    <div class="controls">
                        <textarea  tabindex="2" name="info" class="input-xlarge" id="patient_extra" rows="3" placeholder="Жалобы или дополнительная информация"></textarea>
                    </div>
                </div>
            </fieldset>
            <fieldset>
                <legend>Выбор врача:</legend>
                <div class="control-group" id="doctor_group">
                    <div class="controls">
                        <input type="text" class="input-xlarge" id="doctor_name" placeholder="ФИО врача" value="<?php if(isset($doctor_name)) echo $doctor_name; ?>">
                        <input type="text" class="input span2" id="doctor_specialization" placeholder="Специализация">
                        <span class="help-inline">Врач не найден!</span>
                    </div>
                </div>
                <div class="control-group">
                    <div class="controls">
                        <input type="text" class="input span2" id="datepicker" placeholder="Дата приема">                       
                            <?php echo View::factory('schedule'); ?>
                    </div>
                </div>
            </fieldset>
            <div class="form-actions">
                <button type="submit" class="btn btn-primary">Записать на прием</button>
                <button type="reset" class="btn">Сбросить</button>
            </div>       
            <input type="hidden" name="time" id="time">
            <input type="hidden" name="doctor_id" id="doctor_id">
        </form>
    </div>
        <div class="span4" id="patient_info">  
            
        </div>
        <div class="span4" id="doctor_info">              
        </div>
</div>