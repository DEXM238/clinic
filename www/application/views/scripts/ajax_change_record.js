function get_patient_records(){
    $.ajax({
            type: "POST",
            data: {patient_id: $("#patient_id").val()},
            url: "http://clinic.loc/ajax/get_patient_records",
            dataType: "json",
            success: draw_records
    })
    return false;
}
function draw_records(records){
    $("#patient_records_group").html('');
    if(records == null)
        return false;
    for(var i in records.time){
        $("#patient_records_group").append(
            '<label class="checkbox"><input type="checkbox" name="records[]" value='+records.record_id[i]+'><i class="icon-calendar"></i>&nbsp;'+
            records.date[i]+',&nbsp;<i class="icon-time"></i>&nbsp;'+records.time[i]+
            ', <strong>врач: </strong>'+records.name[i]+',<strong>специализация: </strong>'+
            records.specialization[i]+
            '</label>'
        );
    }
    return false;
}

$(document).ready(function(){
    
    $("#patient_id").blur(function(){
        get_json('short_patient_by_id', $(this).val(), patient_callback);
        get_patient_records();
    });
    $("#patient_name").blur(function(){
        get_json('short_patient_by_name', $(this).val(), patient_callback);
        get_patient_records();
    });   
   

    
    
    $("#patient_id").keypress(function(){
        $("#patient_name").val(null)  
        $("#patient_id").attr('tabindex', '1')
    })
    $("#patient_name").keypress(function(){
        $("#patient_id").val(null)
        $("#patient_id").attr('tabindex', '3')
    })
});