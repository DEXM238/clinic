function doctor_callback(doctor){
    if(doctor == null) {
                    $("#doctor_group").addClass("error").removeClass("success");
                    $("#doctor_specialization").next().css("display", "inline");
                }
                else
                    {
                $("#doctor_specialization").attr("value", doctor.specialization);
                $("#doctor_group").addClass("success").removeClass("error");
                $("#doctor_specialization").next().css("display", "none");
                
                var input = '<em>'+doctor.name+'</em><br><strong>специализация: </strong>'+doctor.specialization;
                input += doctor.category ? '<br><strong>категория: </strong>'+doctor.category : '';
                input += '<br><strong>отделение: </strong>'+doctor.department
                    +'<br><strong>кабинет: </strong>'+doctor.office+'<hr>';                
                $("#doctor_info").html("<h4>Сведения о враче:</h4><hr>").append(input); 
                $("#doctor_id").val(doctor.id);
                time_select();
                
                
                }
    return false;
}

    
function time_select(){
    
    var doctor_id = $("#doctor_id").val();
    var t = new Date();
    t = $("#datepicker").datepicker("getDate");
    var year = t.getFullYear();
  
    var month = t.getMonth()+1;
    month = month > 9 ? month : '0'+month;
    var day = t.getDate();
    day = day > 9 ? day : '0'+day;
    var date = year+'-'+month+'-'+day;
    $("td").removeClass('free').removeClass('occupied');
    
    
    $.ajax({
            type: "POST",
            data: {doctor_id: doctor_id, date: date},
            url: "http://clinic.loc/ajax/get_doctor_shedule",
            dataType: "json",
            success: function(data)
            {
                for(var i in data.time){
                    var cell = data.time[i];
                    $("td:contains('"+cell+"')").addClass('occupied').unbind( "click" ).removeClass('free');
                }
                return false;
            }
    })
    
    $("td:not(.occupied)").addClass('free').unbind( "click" );
    $("td:not(.occupied)").click(function(){
            var time = (date+' '+$(this).html()+':00');
            $("#time").attr("value", time);
            $("td:not(.occupied)").removeClass("success");
            $(this).addClass("success");
            return false; 
        });
    
    
   
}

$(document).ready(function(){
    
    $("#datepicker").datepicker({
       onSelect: time_select
    });
    var today = new Date();

    $("#datepicker").datepicker(
       "setDate" , today
    );
    $("#patient_id").blur(function(){
        get_json('short_patient_by_id', $(this).val(), patient_callback);
    });
    $("#patient_name").blur(function(){
        get_json('short_patient_by_name', $(this).val(), patient_callback);
    });   
   $("#doctor_name").blur(function(){
       get_json('short_doctor_by_name', $(this).val(), doctor_callback);
   });   
   

    
    
    $("#patient_id").keypress(function(){
        $("#patient_name").val(null)  
        $("#patient_id").attr('tabindex', '1')
    })
    $("#patient_name").keypress(function(){
        $("#patient_id").val(null)
        $("#patient_id").attr('tabindex', '3')
    })
});