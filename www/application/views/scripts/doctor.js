function get_schedule()
{
    var doctor_id = 3;
    var t = new Date();
    t = $("#datepicker").datepicker("getDate");
    var year = t.getFullYear();
  
    var month = t.getMonth()+1;
    month = month > 9 ? month : '0'+month;
    var day = t.getDate();
    day = day > 9 ? day : '0'+day;
    var date = year+'-'+month+'-'+day;
  
    $("td").removeClass('occupied').removeClass('success').unbind('click');
     
    $.ajax({
            type: "POST",
            data: {doctor_id: doctor_id, date: date},
            url: "http://clinic.loc/ajax/get_doctor_shedule",
            dataType: "json",
            success: function(data)
            {            
                for(var i in data.time){
                    var cell = data.time[i];
                    var patient = data.patient_id[i];
                    $("td:contains('"+cell+"')").addClass('occupied').data("patient_id", patient).data("info", data.info[i]);
                    $("td:contains('"+cell+"')").click(function(){
                        $("td").removeClass('success');
                        $(this).addClass('success');
                        $("#info").html("<h4>Жалобы или дополнительная информация:</h4><hr>"+$(this).data("info"));
                        $("#patient_id").val($(this).data("patient_id"));
                        get_json('short_patient_by_id', $(this).data("patient_id"), patient_callback);
                    });
                }
                return false;
            }
    })
    return false;
}
    


$(document).ready(function(){
    $("#datepicker").datepicker({
       onSelect: get_schedule
    });
    $("#record").click(function(){
        $('#da1ta').submit(); 
        return false;
    })
    
    $("#showHideContent").click(function (){
        if($("#schedule").is(":hidden")){
            $(this).text('Скрыть расписание')
            $("#schedule").show();
        }else{
            $(this).text('Показать расписание')
            $("#schedule").hide();
        }
        return false;
    });
});
