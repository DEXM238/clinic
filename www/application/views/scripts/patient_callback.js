function patient_callback(patient){
    if(patient == null) {
        $("#patient_group").addClass("error").removeClass("success");
        $("#patient_id").next().css("display", "inline");
    }
    else{
        $("#patient_group").addClass("success").removeClass("error");
        $("#patient_name").attr("value", patient.name);
        $("#patient_id").attr("value", patient.id);
        $("#patient_id").next().css("display", "none");
        $("#patient_info_name").text(patient.name);
        $("#patient_info_gender").append(patient.gender);
        $("#patient_info_birthdate").append(patient.birth_date);
        $("#patient_info_jobtitle").append(patient.job_title);
        var address = 'ул. '+patient.street_title;
        address+= patient.block ? ' блок '+patient.block : '';
        address+= patient.number ? ' д. '+patient.number : '';
        address+= patient.apartment_number ? ' кв. '+patient.apartment_number : '';
        var input = '<em>'+patient.name+'</em><br><strong>Пол:</strong> '+patient.gender
        +'<br><strong>Дата рождения:</strong> '
        +patient.birth_date+'<br><strong>Место работы:</strong> '+patient.job_title;
        input+= patient.street_title ? '<br><i class="icon-home"></i>&nbsp;'+address : '';
        input+= patient.phone ? '<br><i class="icon-font"></i>&nbsp;'+patient.phone : '';
        input+='<hr><br>'
        $("#patient_info").html("<h4>Сведения о пациенте:</h4><hr>").append(input);                
    }
    return false;
}