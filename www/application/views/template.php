<!DOCTYPE html>
<html lang="ru">
    <head>
        <meta charset="utf-8">
        <title><?php echo $title; ?></title>
        <!-- Le HTML5 shim, for IE6-8 support of HTML elements -->
        <!--[if lt IE 9]>
        <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
        <link href="/css/bootstrap.css" rel="stylesheet">
        <link href="/css/main.css" rel="stylesheet">
        <link href="/css/bootstrap-responsive.css" rel="stylesheet">
        <link href="/css/jquery.ui.base.css" rel="stylesheet">
        <link href="/css/jquery.ui.theme.css" rel="stylesheet">
        
        
        <script type="text/javascript" src="/jquery/jquery-1.7.1.min.js"></script>
        <script type="text/javascript" src="/jquery/jquery.ui.widget.min.js"></script>
        <script type="text/javascript" src="/jquery/jquery.ui.core.min.js"></script>
        <script type="text/javascript" src="/jquery/jquery.ui.datepicker.min.js"></script>
        <script type="text/javascript" src="/jquery/jquery.ui.datepicker-ru.js"></script>
        

        <?php foreach($scripts as $script){?> <script type="text/javascript"><?php echo $script; ?></script> <?php }?>
        <style type="text/css">
         body {
            padding-top: 60px;
         }
	</style>
        <link rel="shortcut icon" href="images/favicon.ico">
    </head>
    <body> 
        
        <div class="navbar navbar-fixed-top">
      <div class="navbar-inner">
        <div class="container-fluid">
          <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
            <span class="i-bar"></span>
            <span class="i-bar"></span>
            <span class="i-bar"></span>
          </a>
          <a class="brand" href="#">Клиника <em><strong style="color: #b94a48">PreALPHA</strong></em></a>
          <div class="nav-collapse">
            <ul class="nav">
              <li class="active"><a href="/record">Регистратор</a></li>
              <li><a href="/record/change">Изменение записей</a></li>
              <li><a href="/doctor">Врач</a></li>
              <li><a href="/patient/info">Инфа о пациенте</a></li>
            </ul>
            <p class="navbar-text pull-right">Logged in as <a href="#">username</a></p>
          </div><!--/.nav-collapse -->
        </div>
      </div>
    </div>
        
        <div class="container-fluid">
            
                <?php echo $content; ?>
                 
           
                
            
        </div>
    </body>
</html>