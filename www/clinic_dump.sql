-- MySQL dump 10.13  Distrib 5.5.18, for Win32 (x86)
--
-- Host: localhost    Database: clinic
-- ------------------------------------------------------
-- Server version	5.5.18

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `addresses`
--

DROP TABLE IF EXISTS `addresses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `addresses` (
  `address_id` int(11) NOT NULL AUTO_INCREMENT,
  `street_id` int(11) NOT NULL,
  `block` int(11) DEFAULT NULL,
  `number` int(11) NOT NULL,
  `apartment_number` int(11) DEFAULT NULL,
  PRIMARY KEY (`address_id`),
  KEY `street_id` (`street_id`),
  CONSTRAINT `addresses_ibfk_1` FOREIGN KEY (`street_id`) REFERENCES `streets` (`street_id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `addresses`
--

LOCK TABLES `addresses` WRITE;
/*!40000 ALTER TABLE `addresses` DISABLE KEYS */;
INSERT INTO `addresses` VALUES (1,1,NULL,3,56),(4,1,NULL,14,20),(5,2,NULL,89,100),(6,2,3,2,81),(7,1,NULL,67,34),(8,8,1,5,107),(9,7,NULL,34,20),(10,6,NULL,67,5),(12,12,NULL,6,203),(13,11,NULL,20,44),(14,5,NULL,77,20),(15,5,NULL,3,4),(16,10,NULL,103,44),(17,5,NULL,66,7),(18,8,NULL,23,56),(19,9,2,2,304),(20,11,1,6,3),(21,5,NULL,105,26);
/*!40000 ALTER TABLE `addresses` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `addresses_patients`
--

DROP TABLE IF EXISTS `addresses_patients`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `addresses_patients` (
  `address_id` int(11) NOT NULL,
  `patient_id` int(11) NOT NULL,
  PRIMARY KEY (`address_id`,`patient_id`),
  KEY `patient_id` (`patient_id`),
  CONSTRAINT `addresses_patients_ibfk_1` FOREIGN KEY (`address_id`) REFERENCES `addresses` (`address_id`) ON UPDATE CASCADE,
  CONSTRAINT `addresses_patients_ibfk_2` FOREIGN KEY (`patient_id`) REFERENCES `patients` (`patient_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `addresses_patients`
--

LOCK TABLES `addresses_patients` WRITE;
/*!40000 ALTER TABLE `addresses_patients` DISABLE KEYS */;
INSERT INTO `addresses_patients` VALUES (5,1),(4,2),(6,3),(7,4),(8,5),(1,6),(20,7),(21,8),(10,10),(9,11),(18,15);
/*!40000 ALTER TABLE `addresses_patients` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `allergies`
--

DROP TABLE IF EXISTS `allergies`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `allergies` (
  `allergy_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `allergy_title` varchar(250) NOT NULL,
  PRIMARY KEY (`allergy_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `allergies`
--

LOCK TABLES `allergies` WRITE;
/*!40000 ALTER TABLE `allergies` DISABLE KEYS */;
INSERT INTO `allergies` VALUES (1,'Поллиноз'),(2,'Плесневые грибы'),(3,'Орехи'),(4,'Кунжут'),(5,'Яйца'),(6,'Молоко'),(7,'Мёд'),(8,'Цитрусовые'),(9,'Злаки');
/*!40000 ALTER TABLE `allergies` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `allergies_patients`
--

DROP TABLE IF EXISTS `allergies_patients`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `allergies_patients` (
  `allergy_id` int(11) unsigned NOT NULL,
  `patient_id` int(11) unsigned NOT NULL,
  PRIMARY KEY (`allergy_id`,`patient_id`),
  CONSTRAINT `allergies_patients_ibfk_1` FOREIGN KEY (`allergy_id`) REFERENCES `allergies` (`allergy_id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `allergies_patients`
--

LOCK TABLES `allergies_patients` WRITE;
/*!40000 ALTER TABLE `allergies_patients` DISABLE KEYS */;
INSERT INTO `allergies_patients` VALUES (1,3),(2,3),(3,13),(6,12);
/*!40000 ALTER TABLE `allergies_patients` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `blood_transfusions`
--

DROP TABLE IF EXISTS `blood_transfusions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `blood_transfusions` (
  `blood_transfusion_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `date` date NOT NULL,
  `volume` int(11) NOT NULL,
  `patient_id` int(11) unsigned NOT NULL,
  PRIMARY KEY (`blood_transfusion_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `blood_transfusions`
--

LOCK TABLES `blood_transfusions` WRITE;
/*!40000 ALTER TABLE `blood_transfusions` DISABLE KEYS */;
INSERT INTO `blood_transfusions` VALUES (1,'2010-04-11',500,3),(2,'2010-05-06',1000,3),(3,'2010-04-01',1000,10);
/*!40000 ALTER TABLE `blood_transfusions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cards`
--

DROP TABLE IF EXISTS `cards`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cards` (
  `patient_id` int(10) unsigned NOT NULL,
  `blood` varchar(20) DEFAULT NULL,
  `diabete` char(3) DEFAULT NULL,
  `privilege_id` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`patient_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cards`
--

LOCK TABLES `cards` WRITE;
/*!40000 ALTER TABLE `cards` DISABLE KEYS */;
INSERT INTO `cards` VALUES (1,'I отрицательный','нет',NULL),(2,'II отрицательный','нет',NULL),(3,'II отрицательный','нет',1),(4,'II отрицательный','нет',NULL),(5,'II отрицательный','нет',NULL),(6,'II отрицательный','нет',NULL),(7,'II положительный','нет',5),(8,'II положительный','нет',6),(9,'II положительный','нет',NULL),(10,'II положительный','нет',NULL),(11,'II положительный','нет',NULL),(12,'II положительный','да',NULL),(13,'III отрицательный','да',NULL),(14,'III отрицательный','да',NULL),(15,'III отрицательный','да',NULL),(16,'III отрицательный','нет',NULL),(17,'IV положительный','нет',NULL),(18,'I положительный','нет',NULL),(19,'I положительный','нет',NULL),(20,'IV положительный','нет',NULL);
/*!40000 ALTER TABLE `cards` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contacts`
--

DROP TABLE IF EXISTS `contacts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contacts` (
  `contact_id` int(11) NOT NULL AUTO_INCREMENT,
  `phone` varchar(20) DEFAULT NULL,
  `phone_type` char(1) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`contact_id`),
  UNIQUE KEY `phone` (`phone`,`email`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contacts`
--

LOCK TABLES `contacts` WRITE;
/*!40000 ALTER TABLE `contacts` DISABLE KEYS */;
INSERT INTO `contacts` VALUES (1,'0642234567','Д','sandra@mail.ru'),(2,'0642561189','Д','wendy1@mail.ru'),(3,'0642452311','Д','lori2@gmail.com'),(4,'0642338756','Д','kay0@mail.ru'),(5,'0642875689','Д','victor0@mail.ru'),(6,'0642877782','Д','john22@yandex.ru'),(7,'0642119844','Д','kevin4@yahoo.com'),(8,'0642457698','Р',NULL),(9,'0642875690','Р',NULL),(10,'0642191045','Р',NULL),(11,'0501245566','Д','shane2@mail.ru'),(12,'0506767845','Д','russel@gmail.com'),(13,'0506723109','Р','anton0@gmail.com'),(14,'0667834545','Р','eugene@gmail.com'),(15,'0674567788','Р','jeffrey@gmail.com'),(16,'0501445555','Р',NULL),(17,'0642456789','Д','david19@mail.ru'),(18,'0642677877','Д',NULL),(19,'0642424542','Д',NULL),(20,'0642231111','Д',NULL),(21,'0642136789','Д',NULL),(22,'0642456788','Д',NULL),(23,'0506767888','Р',NULL),(24,'0994567888','Р',NULL);
/*!40000 ALTER TABLE `contacts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contacts_patients`
--

DROP TABLE IF EXISTS `contacts_patients`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contacts_patients` (
  `contact_id` int(11) NOT NULL,
  `patient_id` int(11) NOT NULL,
  PRIMARY KEY (`contact_id`,`patient_id`),
  KEY `patient_id` (`patient_id`),
  CONSTRAINT `contacts_patients_ibfk_1` FOREIGN KEY (`contact_id`) REFERENCES `contacts` (`contact_id`) ON UPDATE CASCADE,
  CONSTRAINT `contacts_patients_ibfk_2` FOREIGN KEY (`patient_id`) REFERENCES `patients` (`patient_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contacts_patients`
--

LOCK TABLES `contacts_patients` WRITE;
/*!40000 ALTER TABLE `contacts_patients` DISABLE KEYS */;
INSERT INTO `contacts_patients` VALUES (10,1),(12,1),(1,2),(5,3),(1,4),(2,5),(6,6),(14,7),(3,8),(4,9),(11,9);
/*!40000 ALTER TABLE `contacts_patients` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `departments`
--

DROP TABLE IF EXISTS `departments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `departments` (
  `department_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `department_name` varchar(100) NOT NULL,
  `description` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`department_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `departments`
--

LOCK TABLES `departments` WRITE;
/*!40000 ALTER TABLE `departments` DISABLE KEYS */;
INSERT INTO `departments` VALUES (1,'Гинекологическое',NULL),(2,'Кардиологическое',NULL),(3,'Отоларингологии',NULL),(4,'Терапевтическое',NULL);
/*!40000 ALTER TABLE `departments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `doctors`
--

DROP TABLE IF EXISTS `doctors`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `doctors` (
  `doctor_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned DEFAULT NULL,
  `sector_id` int(10) unsigned DEFAULT NULL,
  `department_id` int(10) unsigned NOT NULL,
  `specialization` varchar(100) NOT NULL,
  `category` varchar(15) DEFAULT NULL,
  `office` int(10) unsigned NOT NULL,
  `first_name` varchar(100) NOT NULL,
  `middle_name` varchar(100) DEFAULT NULL,
  `last_name` varchar(100) NOT NULL,
  PRIMARY KEY (`doctor_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `doctors`
--

LOCK TABLES `doctors` WRITE;
/*!40000 ALTER TABLE `doctors` DISABLE KEYS */;
INSERT INTO `doctors` VALUES (1,NULL,NULL,1,'Гинеколог',NULL,101,'Анна','Егоровна','Михалина'),(2,NULL,NULL,2,'Диетолог',NULL,204,'Борис','Георгиевич','Охромеев'),(3,NULL,NULL,2,'Кардиолог','вторая',206,'Вячеслав','Федорович','Ерашев'),(4,NULL,NULL,3,'Логопед',NULL,201,'Елена','Евгеньевна','Евлашова'),(5,NULL,NULL,3,'Отоларинголог',NULL,202,'Богдан','Романович','Вешняков'),(6,NULL,1,4,'Педиатр',NULL,711,'Анастасия','Валерьевна','Огуреева'),(7,NULL,1,4,'Терапевт','первая',713,'Николай','Евгеньевич','Самашов');
/*!40000 ALTER TABLE `doctors` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ehrs`
--

DROP TABLE IF EXISTS `ehrs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ehrs` (
  `EHR_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `patient_id` int(10) unsigned NOT NULL,
  `creation_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `creator_id` int(10) unsigned NOT NULL,
  `text` text,
  `author_id` int(10) unsigned NOT NULL,
  `date` datetime NOT NULL,
  `digest` binary(40) NOT NULL,
  `recipe` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`EHR_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ehrs`
--

LOCK TABLES `ehrs` WRITE;
/*!40000 ALTER TABLE `ehrs` DISABLE KEYS */;
/*!40000 ALTER TABLE `ehrs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `infections`
--

DROP TABLE IF EXISTS `infections`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `infections` (
  `infection_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `infection_title` varchar(250) NOT NULL,
  PRIMARY KEY (`infection_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `infections`
--

LOCK TABLES `infections` WRITE;
/*!40000 ALTER TABLE `infections` DISABLE KEYS */;
INSERT INTO `infections` VALUES (1,'Холера'),(2,'Дизентерия'),(3,'Сальмонелёз'),(4,'Столбняк'),(5,'Корь'),(6,'Кандидоз'),(7,'Малярия');
/*!40000 ALTER TABLE `infections` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `infections_patients`
--

DROP TABLE IF EXISTS `infections_patients`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `infections_patients` (
  `infection_id` int(11) unsigned NOT NULL,
  `patient_id` int(11) unsigned NOT NULL,
  PRIMARY KEY (`infection_id`,`patient_id`),
  CONSTRAINT `infections_patients_ibfk_1` FOREIGN KEY (`infection_id`) REFERENCES `infections` (`infection_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `infections_patients`
--

LOCK TABLES `infections_patients` WRITE;
/*!40000 ALTER TABLE `infections_patients` DISABLE KEYS */;
INSERT INTO `infections_patients` VALUES (1,3),(2,3),(6,15),(6,20);
/*!40000 ALTER TABLE `infections_patients` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `jobs`
--

DROP TABLE IF EXISTS `jobs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `jobs` (
  `job_id` int(11) NOT NULL AUTO_INCREMENT,
  `job_title` varchar(100) NOT NULL,
  `status` char(1) NOT NULL,
  PRIMARY KEY (`job_id`),
  UNIQUE KEY `job_title` (`job_title`,`status`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `jobs`
--

LOCK TABLES `jobs` WRITE;
/*!40000 ALTER TABLE `jobs` DISABLE KEYS */;
INSERT INTO `jobs` VALUES (7,'Антей, ЧП','Р'),(14,'Вектор, ООО','Р'),(10,'ВНУ им. В.Даля','Р'),(2,'ВНУ им. В.Даля','У'),(8,'Глобус-Книга, ООО','Р'),(1,'Горизонт, ООО','Р'),(5,'Донбасс Пром Ойл, ООО','Р'),(20,'Донецкхим - химическиц завод, ООО ','Р'),(18,'ДонЭРМ, ОАО','Р'),(9,'Книжный Мир, ЧП','Р'),(4,'Комтех Лтд, ООО','Р'),(13,'ЛНАУ','У'),(12,'ЛНУ им. Т.Шевченка','Р'),(11,'ЛНУ им. Т.Шевченка','У'),(15,'Лото Плюс Диадора, ООО','Р'),(17,'ЛугПромХолод ПКФ, ООО','Р'),(6,'Мобил-Сервис, ЧП','Р'),(21,'СОШ №5','У'),(3,'Спецпромхолдинг, ООО','Р'),(16,'УкрСиббанк, АКИБ','Р'),(19,'ЮгВостокэнергосервис, ООО','Р');
/*!40000 ALTER TABLE `jobs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `medications`
--

DROP TABLE IF EXISTS `medications`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `medications` (
  `medication_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `medication_title` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`medication_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `medications`
--

LOCK TABLES `medications` WRITE;
/*!40000 ALTER TABLE `medications` DISABLE KEYS */;
INSERT INTO `medications` VALUES (1,'Стопартроз'),(2,'Блемарен'),(3,'Чампикс'),(4,'Авелокс'),(5,'Азаран'),(6,'Амоксиклав'),(7,'Сумамед'),(8,'Хемомицин'),(9,'Ципринол');
/*!40000 ALTER TABLE `medications` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `medications_patients`
--

DROP TABLE IF EXISTS `medications_patients`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `medications_patients` (
  `medication_id` int(11) unsigned NOT NULL DEFAULT '0',
  `patient_id` int(11) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`medication_id`,`patient_id`),
  CONSTRAINT `medications_patients_ibfk_1` FOREIGN KEY (`medication_id`) REFERENCES `medications` (`medication_id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `medications_patients`
--

LOCK TABLES `medications_patients` WRITE;
/*!40000 ALTER TABLE `medications_patients` DISABLE KEYS */;
INSERT INTO `medications_patients` VALUES (1,15),(2,3),(2,12),(5,3),(6,3),(7,3);
/*!40000 ALTER TABLE `medications_patients` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `patients`
--

DROP TABLE IF EXISTS `patients`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `patients` (
  `patient_id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(100) NOT NULL,
  `middle_name` varchar(100) DEFAULT NULL,
  `last_name` varchar(100) NOT NULL,
  `gender` char(1) NOT NULL,
  `birth_date` date NOT NULL,
  `job_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`patient_id`),
  KEY `job_id` (`job_id`),
  CONSTRAINT `patients_ibfk_1` FOREIGN KEY (`job_id`) REFERENCES `jobs` (`job_id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `patients`
--

LOCK TABLES `patients` WRITE;
/*!40000 ALTER TABLE `patients` DISABLE KEYS */;
INSERT INTO `patients` VALUES (1,'Федор','Григорьевич','Евлахов','М','1972-05-15',3),(2,'Мария','Федоровна','Артемьева','Ж','1977-06-03',1),(3,'Иван','Русланович','Максаков','М','1964-12-13',3),(4,'Евгения','Геннадиевна','Пушкина','Ж','1972-12-04',5),(5,'Наталья','Павловна','Чернова','Ж','1973-08-09',6),(6,'Егор','Русланович','Щедрин','М','1960-11-18',10),(7,'Василий','Валерьевич','Неретин','М','1980-06-17',14),(8,'Марина','Тимофеевна','Щедрина','Ж','1958-10-19',10),(9,'Анна','Владимировна','Бакулина','Ж','1990-01-19',2),(10,'Никита','Трофимович','Гуров','М','1976-06-20',14),(11,'Ирина','Викторовна','Недогадова','Ж','1971-02-02',17),(12,'Геннадий','Георгиевич','Карамышев','М','1995-04-20',21),(13,'Богдан','Григорьевич','Недоростков','М','1981-02-21',17),(14,'Борис','Владимирович','Лисин','М','1985-04-21',17),(15,'Анна','Михайловна','Микулина','Ж','2000-07-01',21),(16,'Наталья','Владимировна','Комарова','Ж','1974-09-18',20),(17,'Елена','Ивановна','Евланина','Ж','1967-04-21',20),(18,'Никита','Романович','Ширяев','М','1990-07-07',11),(19,'Юлия','Евгеньевна','Мусатова','Ж','1986-03-20',16),(20,'Сергей','Романович','Михаев','М','1977-09-13',16);
/*!40000 ALTER TABLE `patients` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `privileges`
--

DROP TABLE IF EXISTS `privileges`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `privileges` (
  `privilege_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `privilege_title` varchar(250) NOT NULL,
  PRIMARY KEY (`privilege_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `privileges`
--

LOCK TABLES `privileges` WRITE;
/*!40000 ALTER TABLE `privileges` DISABLE KEYS */;
INSERT INTO `privileges` VALUES (1,'інваліди війни'),(2,'учасники війни '),(3,'учасники бойових дій'),(4,'інші інваліди'),(5,'ліквідатори аварії на ЧАЕС'),(6,'евакуйовані'),(7,'жителі, які проживають на території радіоекологічного контролю'),(8,'діти, які народилися від батьків 1-3 груп, постраждалих від аварії на ЧАЕС');
/*!40000 ALTER TABLE `privileges` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `records`
--

DROP TABLE IF EXISTS `records`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `records` (
  `record_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `patient_id` int(10) unsigned NOT NULL,
  `doctor_id` int(10) unsigned NOT NULL,
  `date` datetime NOT NULL,
  `creation_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `info` varchar(300) DEFAULT NULL,
  `status` char(1) NOT NULL,
  PRIMARY KEY (`record_id`),
  UNIQUE KEY `doctor_id` (`doctor_id`,`date`),
  UNIQUE KEY `patient_id_2` (`patient_id`,`date`),
  KEY `patient_id` (`patient_id`,`doctor_id`,`date`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `records`
--

LOCK TABLES `records` WRITE;
/*!40000 ALTER TABLE `records` DISABLE KEYS */;
INSERT INTO `records` VALUES (1,1,3,'2012-04-06 15:30:00','2012-04-11 15:10:29','foobar','A'),(2,15,3,'2012-04-06 12:00:00','2012-04-11 15:17:14','bar','A'),(5,3,8,'2012-04-06 11:15:00','2012-04-11 15:30:44','Перелом ноги','A'),(6,3,20,'2012-04-08 10:00:00','2012-04-11 15:32:21','','A'),(7,8,3,'2012-04-17 09:00:00','2012-04-13 12:54:56','','A'),(8,1,3,'2012-04-06 10:00:00','2012-04-14 12:15:05','','P'),(9,13,3,'2012-04-06 11:15:00','2012-04-14 12:44:45','','A'),(10,2,3,'2012-04-06 11:45:00','2012-04-14 12:45:39','','P'),(11,12,3,'2012-04-15 08:00:00','2012-04-15 16:16:24','','A'),(12,2,3,'2012-04-15 10:00:00','2012-04-15 22:20:29','Боль в голове ;(','A'),(13,10,3,'2012-04-06 09:30:00','2012-04-17 22:13:58','','A'),(14,15,3,'2012-04-18 13:15:00','2012-04-17 22:14:27','ололо','A'),(15,3,3,'2012-04-21 10:45:00','2012-04-18 20:17:29','','A');
/*!40000 ALTER TABLE `records` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `streets`
--

DROP TABLE IF EXISTS `streets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `streets` (
  `street_id` int(11) NOT NULL AUTO_INCREMENT,
  `street_title` varchar(100) NOT NULL,
  `sector_id` int(11) NOT NULL,
  PRIMARY KEY (`street_id`),
  UNIQUE KEY `street_title` (`street_title`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `streets`
--

LOCK TABLES `streets` WRITE;
/*!40000 ALTER TABLE `streets` DISABLE KEYS */;
INSERT INTO `streets` VALUES (1,'Ленина',1),(2,'Леваневского',1),(3,'Лисичанская',1),(4,'Западная',1),(5,'Харьковская',1),(6,'Восточная',2),(7,'Волжская',2),(8,'Калинина',2),(9,'Республиканская',2),(10,'Почтовая',2),(11,'Качалова',3),(12,'Ватутина',3);
/*!40000 ALTER TABLE `streets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping routines for database 'clinic'
--
/*!50003 DROP PROCEDURE IF EXISTS `get_doctor_schedule` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`root`@`localhost`*/ /*!50003 PROCEDURE `get_doctor_schedule`(IN dat CHAR(10), IN doc INT)
BEGIN
    SELECT  DATE_FORMAT(date, '%H:%i') AS time, patient_id, info
    FROM records 
    WHERE (date BETWEEN STR_TO_DATE(CONCAT(dat, ' 00:00:00'), '%Y-%m-%d %H:%i:%s')
    AND STR_TO_DATE(CONCAT(dat, ' 23:59:59'), '%Y-%m-%d %H:%i:%s'))
    AND doctor_id = doc
    AND status = 'A';
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_doctor_shedule` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`root`@`localhost`*/ /*!50003 PROCEDURE `get_doctor_shedule`(IN dat CHAR(10), IN doc INT)
BEGIN
    SELECT  DATE_FORMAT(date, '%H:%i') AS time
    FROM records 
    WHERE (date BETWEEN STR_TO_DATE(CONCAT(dat, ' 00:00:00'), '%Y-%m-%d %H:%i:%s')
    AND STR_TO_DATE(CONCAT(dat, ' 23:59:59'), '%Y-%m-%d %H:%i:%s'))
    AND doctor_id = doc;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_patient_card` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`root`@`localhost`*/ /*!50003 PROCEDURE `get_patient_card`(IN p INT)
BEGIN
    SELECT c.patient_id, c.blood, c.diabete, pr.privilege_title AS privilege,
    GROUP_CONCAT(DISTINCT m.medication_title SEPARATOR ', ') AS medications,
    GROUP_CONCAT(DISTINCT a.allergy_title SEPARATOR ', ') AS allergies,
    GROUP_CONCAT(DISTINCT i.infection_title SEPARATOR ', ') AS infections,
    GROUP_CONCAT(DISTINCT CONCAT('(', DATE_FORMAT(bt.date, '%d.%m.%y'),') ',  bt.volume, ' мл.') SEPARATOR ', ') AS blood_transfusions
    
    FROM cards AS c
    LEFT JOIN medications_patients AS mp
    ON mp.patient_id = c.patient_id
    LEFT JOIN medications AS m
    ON m.medication_id = mp.medication_id
    LEFT JOIN allergies_patients AS ap
    ON ap.patient_id = c.patient_id
    LEFT JOIN allergies AS a
    ON a.allergy_id = ap.allergy_id
    LEFT JOIN infections_patients AS ip
    ON ip.patient_id = c.patient_id
    LEFT JOIN infections AS i
    ON i.infection_id = ip.infection_id
    LEFT JOIN blood_transfusions AS bt
    ON bt.patient_id = c.patient_id
    LEFT JOIN privileges AS pr
    ON pr.privilege_id = c.privilege_id
    WHERE c.patient_id = p
    GROUP BY c.patient_id ;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_patient_records` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`root`@`localhost`*/ /*!50003 PROCEDURE `get_patient_records`(IN p INT)
BEGIN
    SELECT record_id, DATE_FORMAT(date, '%d.%m.%y') AS date,  
    DATE_FORMAT(date, '%H:%i') AS time, 
    CONCAT_WS(' ', d.last_name, d.first_name, d.middle_name) AS name,
    d.specialization
    FROM records AS r
    INNER JOIN doctors AS d
    ON d.doctor_id = r.doctor_id 
    WHERE patient_id = p
    AND status = 'A';
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_short_doctor_by_name` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`root`@`localhost`*/ /*!50003 PROCEDURE `get_short_doctor_by_name`(IN n VARCHAR(300))
BEGIN
    SELECT * FROM (SELECT CONCAT_WS(' ', doc.last_name, doc.first_name, doc.middle_name)  AS name,
    doc.specialization, doc.category, doc.office, dep.department_name AS department, doc.doctor_id AS id
    FROM doctors AS doc
    INNER JOIN departments AS dep
    ON dep.department_id = doc.department_id
    )
    AS temp
    WHERE name = n
    LIMIT 1;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_short_patient` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`root`@`localhost`*/ /*!50003 PROCEDURE `get_short_patient`(IN id INT)
BEGIN
    SELECT CONCAT_WS(' ', p.last_name, p.first_name, p.middle_name) AS name,
        j.job_title, p.gender, p.birth_date,
        a.block, a.number, a.apartment_number, s.street_title,
        c.phone
        FROM patients AS p
        LEFT JOIN jobs AS j ON p.job_id = j.job_id
        LEFT JOIN addresses_patients AS ap ON p.patient_id = ap.patient_id
        LEFT JOIN addresses AS a ON ap.address_id = a.address_id
        LEFT JOIN streets AS s ON s.street_id = a.street_id
        LEFT JOIN contacts_patients AS cp ON cp.patient_id = p.patient_id
        LEFT JOIN contacts AS c ON c.contact_id = cp.contact_id
        WHERE p.patient_id = id
        LIMIT 1;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_short_patient_by_id` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`root`@`localhost`*/ /*!50003 PROCEDURE `get_short_patient_by_id`(IN id INT)
BEGIN
      SELECT CONCAT_WS(' ', p.last_name, p.first_name, p.middle_name) AS name,
        j.job_title, p.gender, p.birth_date,
        a.block, a.number, a.apartment_number, s.street_title,
        c.phone, p.patient_id AS id
        FROM patients AS p
        LEFT JOIN jobs AS j ON p.job_id = j.job_id
        LEFT JOIN addresses_patients AS ap ON p.patient_id = ap.patient_id
        LEFT JOIN addresses AS a ON ap.address_id = a.address_id
        LEFT JOIN streets AS s ON s.street_id = a.street_id
        LEFT JOIN contacts_patients AS cp ON cp.patient_id = p.patient_id
        LEFT JOIN contacts AS c ON c.contact_id = cp.contact_id
        WHERE p.patient_id = id
        LIMIT 1;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_short_patient_by_name` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`root`@`localhost`*/ /*!50003 PROCEDURE `get_short_patient_by_name`(IN n VARCHAR(230))
BEGIN
      SELECT * FROM (
      SELECT p.patient_id, CONCAT_WS(' ', p.last_name, p.first_name, p.middle_name) AS name,
        j.job_title, p.gender, p.birth_date,
        a.block, a.number, a.apartment_number, s.street_title,
        c.phone, p.patient_id AS id
        FROM patients AS p
        LEFT JOIN jobs AS j ON p.job_id = j.job_id
        LEFT JOIN addresses_patients AS ap ON p.patient_id = ap.patient_id
        LEFT JOIN addresses AS a ON ap.address_id = a.address_id
        LEFT JOIN streets AS s ON s.street_id = a.street_id
        LEFT JOIN contacts_patients AS cp ON cp.patient_id = p.patient_id
        LEFT JOIN contacts AS c ON c.contact_id = cp.contact_id
        ) AS temp
        WHERE  name = n
        LIMIT 1;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `ins_record` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`root`@`localhost`*/ /*!50003 PROCEDURE `ins_record`(IN doc INT, IN pat INT, IN inf VARCHAR(300), IN dat CHAR(19))
BEGIN
    INSERT INTO records (patient_id, doctor_id, info, date, status) 
    VALUES (pat, doc, inf, STR_TO_DATE(dat, '%Y-%m-%d %H:%i:%s'), 'A');
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2012-04-27 10:52:23
